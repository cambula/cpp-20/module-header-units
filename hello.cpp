module;
// #include <stdio.h>
// #include <iostream>

export module hello;

import "iostream-proxy.hpp";

export void greeter(const char *name) {
  // printf("Hello %s!\n", name);
  std::cout << "Hello " << name << std::endl;
}
